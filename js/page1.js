// page1.js

// part 1

var Time = new Date();

function showCurrentTime() {
    var myTime = new Date();
    var year = myTime.getFullYear();
    var month = myTime.getMonth() + 1;
    var day = myTime.getDate();
    var hour = myTime.getHours();
    var minute = myTime.getMinutes();

    var strTime = "" + year + "/" + month + "/" + day + "at " + hour + minute;
    return strTime;
}

document.getElementById('paragraph-1-1').innerHTML = strTime;


// part 2

var product1 = [productNumber, description, price, fee, totalAmount];
var product2 = [productNumber, description, price, fee, totalAmount];

class Product {
    constructor(productName, description, price, fee, totalAmount) {
        this.productNumber = productNumber;
        this.description = description;
        this.price = price;
        this.fee = fee;
        this.totalAmount = totalAmount;
    }

    toString() {
        let strInfo = "";
        strInfo = strInfo + "Product Name" + this.productNumber;
        strInfo = strInfo + "Description" + this.description;
        strInfo = strInfo + "Price" + this.price;
        strInfo = strInfo + "Fee" + this.fee;
        strInfo = strInfo + "Total Amount" + this.totalAmount

        return strInfo;
    }
}

var productNumber = ["p010", "p020", "p100", "p200", "p300", "p400"];
var description = ["product 1", "product 2", "product3", "product4", "product 5", "product 6"];
var price = ["200", "100", "100", "200", "300", "150"];
var fee = ["20", "0", "5", "0", "10", "0"];
var totalAmount = ["220", "100", "105", "200", "310", "150"];

var products = [];
products.push(productNumber);
products.push(description);
products.push(price);
products.push(fee);
products.push(totalAmount);


var strTable;
strTable = '<table border="1">';
for (var index = 0; index < products.length; index++) {
    strTable = strTable + "<tr>";

    strTable = strTable + "<td>" + products[index].productName + "</td>";
    strTable = strTable + "<td>" + products[index].description + "</td>";
    strTable = strTable + "<td>" + products[index].price + "</td>";
    strTable = strTable + "<td>" + products[index].fee + "</td>";
    strTable = strTable + "<td>" + products[index].totalAmount + "</td>";

    strTable = strTable + "</tr>";
}
strTable = strTable + '</table>';

document.getElementById('mytable').innerHTML = strTable;