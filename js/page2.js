// page2.js

class Product {
    constructor(productNumber, description, price, fee, totalAmount) {
        this.productNumber = productNumber;
        this.description = description;
        this.price = price;
        this.fee = fee;
        this.totalAmount = totalAmount;
    }
}

var productNumber = new Product();
var description = new Product("Hammer", "Computer", "Monitor", "Tablet", "Mobile");
var price = new Product("$155", "$250", "$360", "$450", "$555");
var fee = new Product();
var totalAmount = new Product();

var products = [];
products.push(productNumber);
products.push(description);
products.push(price);
products.push(fee);
products.push(totalAmount);

function printProductDescription(products) {
    var strList = "<ul>";

    for (var counter = 0; counter < products.length; counter++) {
        strList = strList + "<li>" + products[counter].description + "</li>";
    }

    strList = strList + "</ul>";
    return strList;
}

function printProductPrices(products) {
    var strList = "<ol>";

    for (var counter = 0; counter < products.length; counter++) {
        strList = strList + "<li>" + products[counter].price + "</li>";
    }

    strList = strList + "</ol>";
    return strList;
}

document.getElementById("ulist").innerHTML = printProductDescription(products);
document.getElementById("olist").innerHTML = printProductPrices(products);